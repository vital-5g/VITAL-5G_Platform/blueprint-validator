# Network Application Validator

## Description
Validator for NetApps used in VITAL-5G

## Installation
See the README in the docs folder

## Usage
See the README in the docs folder

## License
For open source projects, say how it is licensed.

## Project status
Under development 

