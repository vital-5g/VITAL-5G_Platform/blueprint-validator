package eu.beia.netapp.validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.web.client.RestTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.beans.factory.annotation.Value;

import com.google.gson.JsonObject;
import java.util.concurrent.TimeUnit;

import eu.beia.netapp.validator.zmq.MainSubscriber;

// @Configuration
// @EnableAutoConfiguration
// @ComponentScan
@SpringBootApplication(scanBasePackages = {
    "eu.beia.netapp.validator"
})

@EnableScheduling
@EntityScan(basePackages = "eu.beia.netapp.validator")
public class Application extends SpringBootServletInitializer {

	private static final Logger log = LoggerFactory.getLogger(Application.class);

    public static final boolean ZMQ = false; // used until integrating with monitoring platform


    protected static void incomingDataHandler(JsonObject jsonObject, String topicName)
    {
        System.out.println(topicName);
        System.out.println(jsonObject);

        // Here will come all the data you are subscribed on.
    }


    // @Bean
    // public RestTemplate restTemplate(RestTemplateBuilder builder) {
    //     return builder.build();
    // }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) { 	
        return application.sources(Application.class, MainSubscriber.class);
    }

    @Value("${crossorigin.origin}")
	public static String crossOrigin;

    public static void main(String[] args) throws Exception {
        SpringApplication.run(Application.class, args);


        
        // // these will have to be moved to the properties file. Keeping them here for reference
        // String serviceMetricPort = "2050";
        // String platformMetricPort = "2051";
        // String networkMetricPort = "2052";
        // String infrastructureMetricPort = "2053";
        // String centrMonPlatfPort = "2054";

        // String topic = "VSIDc56f37fa-10a6-4f28-af64-9aeca964dd91-antwerp-infrastructure-disk_usage-878371ce-93a3-471f-8496-b5575fb5d5ce";
        // String topic2 = "VSIDc56f37fa-10a6-4f28-af64-9aeca964dd91-antwerp-network-LATENCY_USERPLANE_RTT-54";



        // if(ZMQ) {
        //     ZmqSubscriber subscriber = new ZmqSubscriber("172.28.18.102", centrMonPlatfPort);
        //     subscriber.subscribeFirstTime(topic, Application::incomingDataHandler); // When it is the first time you do a
        //     // subscription on your subscriber you always need to use ".subscribeFirstTime" since you need to provide the
        //     // method where you will handle all your incoming data (in this example the "incomingDataHandler" method).
    
        //     subscriber.subscribe(topic2); // When it is your second (or higher) time you want to subscribe to something you
        //     // always use the function "subscribe"
    
    
        //     for(int i=0; i < 20; i++)
        //     {
        //         TimeUnit.SECONDS.sleep(1);
        //     }
    
        //     subscriber.unsubscribe(topic);
        //     System.out.println("unsubscribed");
        //     subscriber.stopSubscriber();
        // }

       

    }
    	
}
