package eu.beia.netapp.validator.controller;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fasterxml.jackson.dataformat.yaml.YAMLGenerator.Feature;
import com.fasterxml.jackson.module.jsonSchema.JsonSchemaGenerator;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.client.RestTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import java.io.IOException;
import java.io.InputStream;
import java.lang.invoke.MethodHandles;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.ValidatorFactory;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.impl.Arguments;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;
import it.nextworks.catalogue.elements.NetAppBlueprint;
import it.nextworks.catalogue.elements.exp.ExpBlueprint;
// import it.nextworks.catalogue.elements.VerticalServiceBlueprint;
import it.nextworks.catalogue.elements.DescriptorInformationElement;
import it.nextworks.catalogue.elements.InterfaceServiceSpec;
import it.nextworks.catalogue.exceptions.MalformattedElementException;
import it.nextworks.catalogue.elements.VsDescriptor;
import it.nextworks.catalogue.elements.RequiredEquipment;
import eu.beia.netapp.validator.ViolationException;
import eu.beia.netapp.validator.models.Response;
import eu.beia.netapp.validator.models.Inventory;
import eu.beia.netapp.validator.models.VerticalSBlueprint;
import eu.beia.netapp.validator.security.SecurityConfig;

// @CrossOrigin(origins = "http://localhost:4200") // we don't use it for now since it doesn't seem to work for the address specified - coulbe for a no. of reasons
@RestController
@Configuration
public class ValidatorController {

  private static final Logger LOG = LoggerFactory.getLogger(ValidatorController.class);

  private static ObjectMapper Y_OBJECT_MAPPER, J_OBJECT_MAPPER;
  private static javax.validation.Validator VALIDATOR;
  
  @Value( "${multisite.inventory.url}" )
  private String inventoryUrl;

  enum TYPE {
    nab,
    iss, // InterfaceServiceSpec//To revisit the TYPE enumeration
    expb,
    vsb,
    vsd
  }

  enum EQUIPMENT { // for testing purposes, the EQUIPMENT will be pulled from the multi-site inventory
    IoT_Supervisor
  }

  @RequestMapping(method = RequestMethod.POST, value = "/api/blueprint/validate")
  public ResponseEntity<Response> processRequest(@RequestBody JsonNode rootNode) {

    Y_OBJECT_MAPPER = new ObjectMapper(new YAMLFactory().configure(Feature.SPLIT_LINES, false))
        .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, true);
    J_OBJECT_MAPPER = new ObjectMapper(new JsonFactory())
        .enable(SerializationFeature.INDENT_OUTPUT);

    ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    VALIDATOR = factory.getValidator();

    final String type = "nab";

    try {
      if (!rootNode.isObject()) {
        throw new IOException("Not a JSON object. Please remove array (-) if present.");
      }
      switch (type) {
        case "nab":
          LOG.info("Selected type: NetApp Blueprint");
          validate(rootNode.toString(), NetAppBlueprint.class, true);
          break;
        case "iss":
          LOG.info("Selected type: InterfaceServiceSpec");
          validate(rootNode.toString(), InterfaceServiceSpec.class, true);
          break;
        case "expb":
          LOG.info("Selected type: Experiment Blueprint");
          validate(rootNode.toString(), ExpBlueprint.class, true);
          break;
        case "vsb":
          LOG.info("Selected type: Vertical Service Blueprint");
          validate(rootNode.toString(), VerticalSBlueprint.class, true);
          break;
        case "vsd":
          LOG.info("Selected type: Vertical  Service Descriptor");
          validate(rootNode.toString(), VsDescriptor.class, true);
          break;
      }
      LOG.info("Validation success");
      Response response = new Response("001", "Blueprint validation success", true);
      return new ResponseEntity<Response>(response, HttpStatus.OK);
    } catch (JsonParseException | JsonMappingException e) {
      LOG.error(e.getOriginalMessage());
      String errorString = e.getOriginalMessage() + "Error at line " + e.getLocation().getLineNr() + ", column "
          + e.getLocation().getColumnNr();
      LOG.error("Error at line {}, column {}", e.getLocation().getLineNr(),
          e.getLocation().getColumnNr());
      Response response = new Response("002", errorString, false);
      return new ResponseEntity<Response>(response, HttpStatus.OK);
    } catch (ViolationException e) {
      for (String v : e.getViolationMessages()) {
        LOG.error(v);
      }
      Response response = new Response("003", "e.getViolationMessages()", false); //ToDo this returns a set of Strings. Have to make a JSON array of responses or concat the strings
      return new ResponseEntity<Response>(response, HttpStatus.OK);
    } catch (MalformattedElementException e) {
      LOG.error(e.getMessage());
      Response response = new Response("004", e.getMessage(), false);
      return new ResponseEntity<Response>(response, HttpStatus.OK);
    } catch (IOException e) {
      LOG.error("Can't validate JSON. {}", e.getMessage());
      String errorString = "Can't validate JSON. " + e.getMessage();
      Response response = new Response("005", errorString, false);
      return new ResponseEntity<Response>(response, HttpStatus.OK);
    }

  }

  @RequestMapping(method = RequestMethod.POST, value = "/api/blueprint/validate/syntax")
  public ResponseEntity<Response> processSyntaxRequest(@RequestBody JsonNode rootNode) {

    Y_OBJECT_MAPPER = new ObjectMapper(new YAMLFactory().configure(Feature.SPLIT_LINES, false))
        .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, true);
    J_OBJECT_MAPPER = new ObjectMapper(new JsonFactory())
        .enable(SerializationFeature.INDENT_OUTPUT);

    ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    VALIDATOR = factory.getValidator();

    final String type = "nab";

    try {
      if (!rootNode.isObject()) {
        throw new IOException("Not a JSON object. Please remove array (-) if present.");
      }
      switch (type) {
        case "nab":
          LOG.info("Selected type: NetApp Blueprint");
          validate(rootNode.toString(), NetAppBlueprint.class, true);
          break;
        case "iss":
          LOG.info("Selected type: InterfaceServiceSpec");
          validate(rootNode.toString(), InterfaceServiceSpec.class, true);
          break;
        case "expb":
          LOG.info("Selected type: Experiment Blueprint");
          validate(rootNode.toString(), ExpBlueprint.class, true);
          break;
        case "vsb":
          LOG.info("Selected type: Vertical Service Blueprint");
          validate(rootNode.toString(), VerticalSBlueprint.class, true);
          break;
        case "vsd":
          LOG.info("Selected type: Vertical  Service Descriptor");
          validate(rootNode.toString(), VsDescriptor.class, true);
          break;
      }
      LOG.info("Validation success");
      Response response = new Response("001", "NetApp Blueprint validation success", true);
      return new ResponseEntity<Response>(response, HttpStatus.OK);
    } catch (JsonParseException | JsonMappingException e) {
      LOG.error(e.getOriginalMessage());
      String errorString = e.getOriginalMessage() + "Error at line " + e.getLocation().getLineNr() + ", column "
          + e.getLocation().getColumnNr();
      LOG.error("Error at line {}, column {}", e.getLocation().getLineNr(),
          e.getLocation().getColumnNr());
      Response response = new Response("002", errorString, false);
      return new ResponseEntity<Response>(response, HttpStatus.OK);
    } catch (ViolationException e) {
      for (String v : e.getViolationMessages()) {
        LOG.error(v);
      }
      Response response = new Response("003", "e.getViolationMessages()", false); //ToDo this returns a set of Strings. Have to make a JSON array of responses or concat the strings
      return new ResponseEntity<Response>(response, HttpStatus.OK);
    } catch (MalformattedElementException e) {
      LOG.error(e.getMessage());
      Response response = new Response("004", e.getMessage(), false);
      return new ResponseEntity<Response>(response, HttpStatus.OK);
    } catch (IOException e) {
      LOG.error("Can't validate JSON. {}", e.getMessage());
      String errorString = "Can't validate JSON. " + e.getMessage();
      Response response = new Response("005", errorString, false);
      return new ResponseEntity<Response>(response, HttpStatus.OK);
    }
  }

  @RequestMapping(method = RequestMethod.POST, value="/api/blueprint/validate/sensors")
  public ResponseEntity<Response> processSensorRequest(@RequestBody JsonNode rootNode) {

      Y_OBJECT_MAPPER = new ObjectMapper(new YAMLFactory().configure(Feature.SPLIT_LINES, false))
          .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, true);
      J_OBJECT_MAPPER = new ObjectMapper(new JsonFactory())
          .enable(SerializationFeature.INDENT_OUTPUT);
  
      ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
      VALIDATOR = factory.getValidator();

      NetAppBlueprint blueprint = new NetAppBlueprint();

      
      try {
        blueprint = J_OBJECT_MAPPER.treeToValue(rootNode, NetAppBlueprint.class); 
      } catch (JsonProcessingException e) {
        LOG.error(e.getOriginalMessage());
        String errorString = e.getOriginalMessage() + "Error at line " + e.getLocation().getLineNr() + ", column " + e.getLocation().getColumnNr();
        LOG.error("Error at line {}, column {}", e.getLocation().getLineNr(),
            e.getLocation().getColumnNr());
        Response response = new Response("006", errorString, false);
        return new ResponseEntity<Response>(response, HttpStatus.OK);
      }

      //Need to figure out a way to optimize this code ?? 
      List<RequiredEquipment> requiredEquipmentList = new ArrayList<RequiredEquipment>();
      requiredEquipmentList = blueprint.getRequiredEquipments();

      RequiredEquipment[] requiredEquipmentArr = requiredEquipmentList.stream().toArray(RequiredEquipment[]::new);

      List<String> requiredEquipmentArray = Arrays.stream(requiredEquipmentArr)
      .map(RequiredEquipment::getRequiredEquipmentId)
      .collect(Collectors.toList());

      List<Boolean> requiredEquipmentMandatoryArray = Arrays.stream(requiredEquipmentArr)
      .map(RequiredEquipment::isMandatory)
      .collect(Collectors.toList());

      HttpHeaders headers = new HttpHeaders();
      headers.set("Accept", "application/json");
      HttpEntity<Void> requestEntity = new HttpEntity<>(headers);

      RestTemplate restTemplate = new RestTemplate();

      ResponseEntity<Inventory[]> inventoryResponseEntity = restTemplate.exchange(inventoryUrl, HttpMethod.GET, requestEntity, Inventory[].class);

      Inventory[] inventoryList = inventoryResponseEntity.getBody();

      List<String> equipmentArray = Arrays.stream(inventoryList)
      .map(Inventory::getEquipmentId)
      .collect(Collectors.toList());

      LOG.debug(equipmentArray.get(0)); // ToDo: 

      /**
      List<String> equipmentArray = new ArrayList<String>(
        Arrays.asList("aisTransponder","iotSensor","libeliumStation")
      );
      */

      try {
        if (!rootNode.isObject()) {
          throw new IOException("Not a JSON object. Please remove array (-) if present.");
        }

        for (int i=0; i<requiredEquipmentArray.size(); i++) {
          for (int j=0; j<equipmentArray.size(); j++)
          {
            if (requiredEquipmentArray.get(i).equals(equipmentArray.get(j))) { // we found the equipment in the list,moving on to next equipment
              break;
            }
            if (j == (equipmentArray.size()-1)) { // I am at the last line and no equipment has been found
              if (requiredEquipmentMandatoryArray.get(i)) { //If the equipment is mandatory then we have to throw exception as blueprint cannot be onboarded
                throw new IOException("Required Equipment id: " + requiredEquipmentArray.get(i) + " not available in the testbed");
              }
            }
          }
        }

        Response response = new Response("001", "NetApp Blueprint validation success", true);
        return new ResponseEntity<Response>(response, HttpStatus.OK);

      } catch (JsonParseException | JsonMappingException e) {
        LOG.error(e.getOriginalMessage());
        String errorString = e.getOriginalMessage() + "Error at line " + e.getLocation().getLineNr() + ", column " + e.getLocation().getColumnNr();
        LOG.error("Error at line {}, column {}", e.getLocation().getLineNr(),
            e.getLocation().getColumnNr());
        Response response = new Response("002", errorString, false);
        return new ResponseEntity<Response>(response, HttpStatus.OK);
      } /** catch (ViolationException e) {
        for (String v : e.getViolationMessages()) { //ToDo check if these are needed
          LOG.error(v);
        }
        Response response = new Response("003", "e.getViolationMessages()", false);
        return new ResponseEntity<Response>(response, HttpStatus.OK);
      } catch (MalformattedElementException e) {
        LOG.error(e.getMessage());
        Response response = new Response("004", e.getMessage(), false);
        return new ResponseEntity<>(response, HttpStatus.OK);
      } */ catch (IOException e) {
         LOG.error("Can't validate JSON. {}", e.getMessage());
         String errorString = "Can't validate JSON. " + e.getMessage();
         Response response = new Response("005", errorString, false);
         return new ResponseEntity<Response>(response, HttpStatus.OK);
      }

  }

  @RequestMapping(method = RequestMethod.POST, value = "/api/blueprint/vsb/validate/syntax")
  public ResponseEntity<Response> processVsbSyntaxRequest(@RequestBody JsonNode rootNode) {

    Y_OBJECT_MAPPER = new ObjectMapper(new YAMLFactory().configure(Feature.SPLIT_LINES, false))
        .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, true);
    J_OBJECT_MAPPER = new ObjectMapper(new JsonFactory())
        .enable(SerializationFeature.INDENT_OUTPUT);

    ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    VALIDATOR = factory.getValidator();

    final String type = "vsb";

    try {
      if (!rootNode.isObject()) {
        throw new IOException("Not a JSON object. Please remove array (-) if present.");
      }
      switch (type) {
        case "nab":
          LOG.info("Selected type: NetApp Blueprint");
          validate(rootNode.toString(), NetAppBlueprint.class, true);
          break;
        case "iss":
          LOG.info("Selected type: InterfaceServiceSpec");
          validate(rootNode.toString(), InterfaceServiceSpec.class, true);
          break;
        case "expb":
          LOG.info("Selected type: Experiment Blueprint");
          validate(rootNode.toString(), ExpBlueprint.class, true);
          break;
        case "vsb":
          LOG.info("Selected type: Vertical Service Blueprint");
          validate(rootNode.toString(), VerticalSBlueprint.class, true);
          break;
        case "vsd":
          LOG.info("Selected type: Vertical  Service Descriptor");
          validate(rootNode.toString(), VsDescriptor.class, true);
          break;
      }
      LOG.info("Validation success");
      Response response = new Response("001", "Vertical Service Blueprint validation success", true);
      return new ResponseEntity<Response>(response, HttpStatus.OK);
    } catch (JsonParseException | JsonMappingException e) {
      LOG.error(e.getOriginalMessage());
      String errorString = e.getOriginalMessage() + "Error at line " + e.getLocation().getLineNr() + ", column "
          + e.getLocation().getColumnNr();
      LOG.error("Error at line {}, column {}", e.getLocation().getLineNr(),
          e.getLocation().getColumnNr());
      Response response = new Response("002", errorString, false);
      return new ResponseEntity<Response>(response, HttpStatus.OK);
    } catch (ViolationException e) {
      for (String v : e.getViolationMessages()) {
        LOG.error(v);
      }
      Response response = new Response("003", "e.getViolationMessages()", false); //ToDo this returns a set of Strings. Have to make a JSON array of responses or concat the strings
      return new ResponseEntity<Response>(response, HttpStatus.OK);
    } catch (MalformattedElementException e) {
      LOG.error(e.getMessage());
      Response response = new Response("004", e.getMessage(), false);
      return new ResponseEntity<Response>(response, HttpStatus.OK);
    } catch (IOException e) {
      LOG.error("Can't validate JSON. {}", e.getMessage());
      String errorString = "Can't validate JSON. " + e.getMessage();
      Response response = new Response("005", errorString, false);
      return new ResponseEntity<Response>(response, HttpStatus.OK);
    }
  }

  /**
   * @param s      string containing the blueprint to be validated
   * @param cls    the specific blueprint class to use for validation (e.g.
   *               VsBlueprint.class)
   * @param schema if true, generate JSON schema for cls
   * @param <T>    The type of the class to be validated
   * @throws IOException                  if Jackson fails to deserialize the
   *                                      blueprint; it can be
   *                                      JsonParseException or
   *                                      JsonMappingException
   * @throws ViolationException           if javax.validation fails
   * @throws MalformattedElementException if call to isValid() fails
   */
  private static <T extends DescriptorInformationElement> void validate(String s, Class<T> cls,
      boolean schema)
      throws IOException, ViolationException, MalformattedElementException {
    if (schema) {
      LOG.info("Schema:\n{}",
          J_OBJECT_MAPPER
              .writeValueAsString(new JsonSchemaGenerator(J_OBJECT_MAPPER).generateSchema(cls)));
    }
    T b = Y_OBJECT_MAPPER.readValue(s, cls);
    LOG.debug("Dump:\n{}", Y_OBJECT_MAPPER.writeValueAsString(b));
    Set<ConstraintViolation<T>> violations = VALIDATOR.validate(b);
    if (!violations.isEmpty()) {
      throw new ViolationException(violations);
    }
    b.isValid();
  }
}
