package eu.beia.netapp.validator.models;

public class Response {
    private String id;
    private String validationString;
    private boolean isValid;

    public Response(String id, String validationString, boolean isValid) {
        this.id = id;
        this.validationString = validationString;
        this.isValid = isValid;
    }
    
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getValidationString() {
        return validationString;
    }
    public void setValidationString(String validationString) {
        this.validationString = validationString;
    }

    public boolean isValid() {
        return isValid;
    }
    public void setValid(boolean isValid) {
        this.isValid = isValid;
    }
}
