package eu.beia.netapp.validator.models.infrastructure;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Latency {
    private String timestamp;
    private String networkSliceIdString;
    private String unit;

    public Latency(String timestamp, String networkSliceIdString, String unit, String value) {
        this.timestamp = timestamp;
        this.networkSliceIdString = networkSliceIdString;
        this.unit = unit;
        this.value = value;
    }

    public String getTimestamp() {
        return this.timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getNetworkSliceIdString() {
        return this.networkSliceIdString;
    }

    public void setNetworkSliceIdString(String networkSliceIdString) {
        this.networkSliceIdString = networkSliceIdString;
    }

    public String getUnit() {
        return this.unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(String value) {
        this.value = value;
    }
    private String value;


}
