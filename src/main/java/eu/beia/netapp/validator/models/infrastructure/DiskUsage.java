package eu.beia.netapp.validator.models.infrastructure;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DiskUsage {
        
    private String timestamp;
    private String vmId;
    private String unit;
    private String value;

    public DiskUsage(String timestamp, String vmId, String unit, String value) {
        this.timestamp = timestamp;
        this.vmId = vmId;
        this.unit = unit;
        this.value = value;
    }


    public String getTimestamp() {
        return timestamp;
    }
    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
    public String getVmId() {
        return vmId;
    }
    public void setVmId(String vmId) {
        this.vmId = vmId;
    }
    public String getUnit() {
        return unit;
    }
    public void setUnit(String unit) {
        this.unit = unit;
    }
    public String getValue() {
        return value;
    }
    public void setValue(String value) {
        this.value = value;
    }

    
}
