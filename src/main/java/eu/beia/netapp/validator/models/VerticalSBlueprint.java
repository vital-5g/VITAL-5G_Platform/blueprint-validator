package eu.beia.netapp.validator.models;

import it.nextworks.catalogue.elements.VerticalServiceBlueprint;
import it.nextworks.catalogue.elements.DescriptorInformationElement;
import it.nextworks.catalogue.exceptions.MalformattedElementException;

public class VerticalSBlueprint implements DescriptorInformationElement {

    private VerticalServiceBlueprint vsBlueprint;

    public VerticalSBlueprint() {

    }

    public VerticalSBlueprint(VerticalServiceBlueprint vsBlueprint) {
        this.vsBlueprint = vsBlueprint;
    }

    public VerticalServiceBlueprint getVsBlueprint() {
        return vsBlueprint;
    }

    public void setVsBlueprint(VerticalServiceBlueprint vsBlueprint) {
        this.vsBlueprint = vsBlueprint;
    }

    @Override
    public void isValid() throws MalformattedElementException {
        if(vsBlueprint==null) throw new MalformattedElementException("Vertical Service Blueprint without vsBlueprint element");
       
        vsBlueprint.isValid();
    }
    
}
