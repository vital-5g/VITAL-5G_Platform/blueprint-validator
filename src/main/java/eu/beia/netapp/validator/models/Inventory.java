package eu.beia.netapp.validator.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Inventory {
    private String equipmentRecordId;
    private String accessLevel;
    private String equipmentId;
    private String location;
    private String type;
    private String subType;
    private String testbed;

    public Inventory(@JsonProperty("equipmentRecordId") String equipmentRecordId, @JsonProperty("accessLevel") String accessLevel,  @JsonProperty("equipmentId") String equipmentId, @JsonProperty("location") String location, @JsonProperty("type") String type,
    @JsonProperty("subType") String subType, @JsonProperty("testbed") String testbed) {
        this.equipmentRecordId = equipmentRecordId;
        this.accessLevel = accessLevel;
        this.equipmentId = equipmentId;
        this.location = location;
        this.type = type;
        this.subType = subType;
        this.testbed = testbed;
    }

    public String getEquipmentRecordId() {
        return this.equipmentRecordId;
    }

    public void setEquipmentRecordId(String input) {
        this.equipmentRecordId = input;
    }

    public String getAccessLevel() {
        return this.accessLevel;
    }

    public void setAccessLevel(String input) {
        this.accessLevel = input;
    }

    public String getEquipmentId() {
        return this.equipmentId;
    }

    public void setEquipmentId(String input) {
        this.equipmentId = input;
    }

    public String getLocation() {
        return this.location;
    }

    public void setLocation(String input) {
        this.location = input;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String input) {
        this.type = input;
    }
        
    public String getSubType() {
        return this.subType;
    }

    public void setSubType(String input) {
        this.subType = input;
    }

    public String getTestbed() {
        return this.testbed;
    }

    public void setTestbed(String input) {
        this.testbed = input;
    }

}