package eu.beia.netapp.validator.zmq;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;

import eu.beia.netapp.validator.models.infrastructure.DiskUsage;
import eu.beia.netapp.validator.zmq.ZmqSubscriber;

import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.web.client.RestTemplate;
import org.springframework.http.ResponseEntity;

import org.springframework.scheduling.annotation.Scheduled;

import org.springframework.stereotype.Component;

@Component
public class MainSubscriber {

    @Value("${monitoring.platform.service-metric.port}")
    private String serviceMetricPort;

    @Value("${monitoring.platform.platform-metric.port}")
    private String platformMetricPort;

    @Value("${monitoring.platform.network-metric.port}")
    private String networkMetricPort;

    @Value("${monitoring.platform.infrastructure-metric.port}")
    private String infrastructureMetricPort;

    @Value("${monitoring.platform.port}")
    private String centrMonPlatfPort;

    @Value("${monitoring.platform.url}")
    private String centrMonPlatfUrl;

    @Value("${experiment-lcm.url}")
    private static String experimentLcmUrl;
    
    protected static void incomingDataHandler(JsonObject jsonObject, String topicName)
    {

        // Here you will come all the data you are subscribed on.

        float diskUsageThreshold = 50f; // in % hardcoded for now
        float latencyThreshold = 15f; // in ms hardcoded for now

        String[] topicParts = topicName.split("-");
        String id = topicParts[0];
        String testbed = topicParts[1];
        String metric = topicParts[2];
        String kpi = topicParts[3];
        String vmId = topicParts[4];
        


        switch (kpi) {
            case "disk_usage": 

                String diskUsageValueString = jsonObject.get("value").toString();
                float diskUsagevalue = Float.parseFloat(diskUsageValueString);

                if (diskUsagevalue>diskUsageThreshold) {
                    //ToDo: call experiment LCM to terminate experiment

                    HttpHeaders headers = new HttpHeaders();
                    headers.set("Accept", "application/json");
                    HttpEntity<Void> requestEntity = new HttpEntity<>(headers);

                    String urlTemplate = UriComponentsBuilder.fromHttpUrl(experimentLcmUrl)
                        .path("/portal/elcm/experiment/"+ id)
                        .encode()
                        .toUriString();

                    RestTemplate restTemplate = new RestTemplate();

                    ResponseEntity<String> response = restTemplate.exchange(urlTemplate, HttpMethod.DELETE, requestEntity, String.class);

                }
                break;
            case "LATENCY_USERPLANE_RTT":
                
                String latencyValueString = jsonObject.get("value").toString();
                float latencyValue = Float.parseFloat(latencyValueString);

                if (latencyValue>latencyThreshold) {
                    //ToDo: call experiment LCM to terminate experiment
                    HttpHeaders headers = new HttpHeaders();
                    headers.set("Accept", "application/json");
                    HttpEntity<Void> requestEntity = new HttpEntity<>(headers);

                    String urlTemplate = UriComponentsBuilder.fromHttpUrl(experimentLcmUrl)
                        .path("/portal/elcm/experiment/"+ id)
                        .encode()
                        .toUriString();

                    RestTemplate restTemplate = new RestTemplate();

                    ResponseEntity<String> response = restTemplate.exchange(urlTemplate, HttpMethod.DELETE, requestEntity, String.class);

                }
                break;

        }
    }

    // @PostConstruct
    
    public void init() throws InterruptedException {

        // receiveMessage();
        // is this needed?

    }

    @Scheduled(fixedDelay = 60000, initialDelay = 60000)
    public void receiveMessage() throws InterruptedException {

        // System.out.println("we are receiving the message");
        // these will have to be moved to the properties file. Keeping them here for reference


        String topic = "VSIDc56f37fa-10a6-4f28-af64-9aeca964dd91-antwerp-infrastructure-disk_usage-878371ce-93a3-471f-8496-b5575fb5d5ce";
        String topic2 = "VSIDc56f37fa-10a6-4f28-af64-9aeca964dd91-antwerp-network-LATENCY_USERPLANE_RTT-54";


        ZmqSubscriber subscriber = new ZmqSubscriber(centrMonPlatfUrl, centrMonPlatfPort);
        subscriber.subscribeFirstTime(topic, MainSubscriber::incomingDataHandler); // When it is the first time you do a
        // subscription on your subscriber you always need to use ".subscribeFirstTime" since you need to provide the
        // method where you will handle all your incoming data (in this example the "incomingDataHandler" method).

        subscriber.subscribe(topic2); // When it is your second (or higher) time you want to subscribe to something you
        // always use the function "subscribe"


        for(int i=0; i < 20; i++)
        {
            TimeUnit.SECONDS.sleep(1);
        }

        subscriber.unsubscribe(topic);
        System.out.println("unsubscribed");
        subscriber.stopSubscriber();
    }
}
