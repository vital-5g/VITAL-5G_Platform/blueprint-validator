# blueprint-validator
Static and dynamic validator for blueprints used in VITAL-5G

### Command line usage

The war file produced by `mvn install` has to be deployed in a (Tomcat) server and can be used to validate blueprints.

The blueprints can be validated by providing the desired JSON blueprint file to the API call

Usage example:

```
# validate a Network Application Blueprint syntax
curl -X POST http://localhost:8080/validator/api/blueprint/validate/syntax -H "Content-Type: application/json" -d @blueprint.json

# validate list of sensors required by Network Application (calls multisite inventory)
curl -X POST http://localhost:8080/validator/api/blueprint/validate/sensors -H "Content-Type: application/json" -d @blueprint.json

# validate both syntax and list of sensors required by Network Application (calls multisite inventory)
curl -X POST http://localhost:8080/validator/api/blueprint/validate -H "Content-Type: application/json" -d @blueprint.json

# validate vertical service blueprint
curl -X POST http://localhost:8080/validator/api/vs/blueprint/validate -H "Content-Type: application/json" -d @blueprint.json


```

